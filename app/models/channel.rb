class Channel < ApplicationRecord
  has_many :messages

  validates :name, presence: true, uniqueness: true

  def upper_name
    name.upcase
  end
end
